from distutils.core import setup

setup(
    name='reddit_stream',
    version='0.1',
    description='A Python package for processing Reddit comments as soon as they are posted.',
    author='Paul Nechifor',
    url='https://github.com/paul-nechifor/reddit_stream',
    packages=['reddit_stream'],
)
